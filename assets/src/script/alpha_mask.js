
/*******************************************************************
"alpha_mask" Component:
This component can be applied any node that has cc.Sprite component on it to mask with alpha texture.
It compiles a shader to get RGB colors from original sprite texture and ALPHA from alpha texture.
Useful to apply transparency on JPEG images.

Tested on: Cocos Creator 1.9.3 Editor, Simulator, Browser, Windows, Android.

** Note for browser and editor:
It doesn't work properly if original sprite frame and alpha sprite frame comes from differen texture atlases.
I couldn't manage to give second texture on webgl. Any contribution appreciated :)

Martin
Super Mega Space Game! Dev Team
supermegaspacegame.com
********************************************************************/

// for webgl
var _default_vert = 
`
attribute vec4 a_position;
attribute vec2 a_texCoord;
attribute vec4 a_color;
varying vec2 v_texCoord;
varying vec4 v_fragmentColor;
void main()
{
    gl_Position = CC_PMatrix * a_position;
    v_fragmentColor = a_color;
    v_texCoord = a_texCoord;
}
`;

var _alpha_mask_frag = 
`
#ifdef GL_ES
precision mediump float;
#endif
uniform sampler2D alpha_texture;
uniform vec2 tex_offset;
uniform vec2 tex_ratio;
varying vec2 v_texCoord;

void main()
{
    vec3 color = texture2D(CC_Texture0, v_texCoord).rgb;
    vec3 alpha = texture2D(alpha_texture, (v_texCoord - tex_offset) * tex_ratio ).rgb;
    
    gl_FragColor = vec4(color.r, color.g, color.b, alpha.r);
}
`;

cc.Class({
    extends: cc.Component,

    editor : CC_EDITOR && {
        executeInEditMode : true,
        menu: 'SMSG/Util/alpha_mask',
    },

    properties: {
        Alpha_Texture:{
            default:null,
            type:cc.SpriteFrame,
            tooltip:"Set grayscale image for alpha channel (black=transparent)",
            notify(){
                this._use();
            },
        }
    },

    onLoad: function () {

        this.tex_offset = cc.v2();
        this.tex_ratio = cc.v2();

        this._use();
    },


    _use: function()
    {
        this._program = new cc.GLProgram();
        if (cc.sys.isNative) {
            this._program.initWithString(_default_vert, _alpha_mask_frag);
            this._program.link();
            this._program.updateUniforms();
        }else{
            this._program.initWithVertexShaderByteArray(_default_vert, _alpha_mask_frag);
            this._program.addAttribute(cc.macro.ATTRIBUTE_NAME_POSITION, cc.macro.VERTEX_ATTRIB_POSITION);
            this._program.addAttribute(cc.macro.ATTRIBUTE_NAME_COLOR, cc.macro.VERTEX_ATTRIB_COLOR);
            this._program.addAttribute(cc.macro.ATTRIBUTE_NAME_TEX_COORD, cc.macro.VERTEX_ATTRIB_TEX_COORDS);
            this._program.link();
            this._program.updateUniforms();
            this._program.use();
        }


        let Color_Sprite_Frame = this.node.getComponent(cc.Sprite).spriteFrame;
        let Alpha_Sprite_Frame = this.Alpha_Texture;

        let Color_Sprite_Rect = Color_Sprite_Frame.getRect();
        let Alpha_Sprite_Rect = Alpha_Sprite_Frame.getRect();

        let Color_Texture = Color_Sprite_Frame.getTexture();
        let Alpha_Texture = Alpha_Sprite_Frame.getTexture();

        // If texture sizes are different
        this.tex_ratio.x = Color_Texture.width / Alpha_Texture.width;
        this.tex_ratio.y = Color_Texture.height / Alpha_Texture.height;
        
        this.tex_offset.x = (Color_Sprite_Rect.x / Color_Texture.width)  - (Alpha_Sprite_Rect.x / Alpha_Texture.width );
        this.tex_offset.y = (Color_Sprite_Rect.y / Color_Texture.height) - (Alpha_Sprite_Rect.y / Alpha_Texture.height );
        
        if (cc.sys.isNative) {
            var glProgram_state = cc.GLProgramState.getOrCreateWithGLProgram(this._program);
            glProgram_state.setUniformTexture( "alpha_texture", this.Alpha_Texture.getTexture()._glID );
            glProgram_state.setUniformVec2( "tex_offset", this.tex_offset );
            glProgram_state.setUniformVec2( "tex_ratio", this.tex_ratio );
        }else{

            this._program.setUniformLocationWith1i( "alpha_texture", this.Alpha_Texture.getTexture()._glID );
            this._program.setUniformLocationWith2f( "tex_offset" , this.tex_offset.x , this.tex_offset.y );
            this._program.setUniformLocationWith2f( "tex_ratio" , this.tex_ratio.x , this.tex_ratio.y ); 
        }

        this.setProgram( this.node._sgNode, this._program );

    },
    setProgram:function (node, program) {

        if (cc.sys.isNative) {
            var glProgram_state = cc.GLProgramState.getOrCreateWithGLProgram(program);
            node.setGLProgramState(glProgram_state);
        }else{
            node.setShaderProgram(program);    
        }
        
        // Need to apply Scale9Sprite child node
        var children = node.children;
        if (!children)
            return;
    
        for (var i = 0; i < children.length; i++){
            if(children[i].constructor === cc.Scale9Sprite){ // apply only Scale9Sprite nodes
                this.setProgram(node.children[i], program);
            }
        }
    },

    
});



